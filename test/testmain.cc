// *** CPSC 4210/5210 Assignment 4 code
// *** main file for the packet construction test cases
// Group Pi4
// Cody Barnson
// Dustin Fowler
// Andrew Chichak
// Husain Syed

#include <iostream>
#include <memory>

#include <cppunit/TextTestRunner.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestListener.h>
#include <cppunit/extensions/TestSuiteBuilderContext.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/ui/text/TextTestRunner.h>

#include <ConcreteIPUDPPacketFactory.h>
#include <IPUDPPacketFactory.h>
#include <ConcreteOLSRPacketFactory.h>
#include <OLSRPacketFactory.h>
#include <ConcreteOLSRHelloFactory.h>
#include <OLSRHelloFactory.h>

// Include the test fixture header files here
#include "TestIPUDPPacketPi4.h"
#include "TestOLSRPacketPi4.h"
#include "TestOLSRHelloPi4.h"

// Initialize the static factory member of the test fixtures here
std::shared_ptr<IPUDPPacketFactory> TestIPUDPPacketPi4::factory =
   std::make_shared<ConcreteIPUDPPacketFactory>();

// Initialize the static factory member of the test fixtures here
std::shared_ptr<OLSRPacketFactory> TestOLSRPacketPi4::factory =
   std::make_shared<ConcreteOLSRPacketFactory>();

// Initialize the static factory member of the test fixtures here
std::shared_ptr<OLSRHelloFactory> TestOLSRHelloPi4::factory =
   std::make_shared<ConcreteOLSRHelloFactory>();

int main() {
   CppUnit::TextUi::TestRunner runner;
   runner.addTest(TestIPUDPPacketPi4::suite());
   runner.addTest(TestOLSRPacketPi4::suite());
   runner.addTest(TestOLSRHelloPi4::suite());
   // add other test suites here like on the line above
   runner.run();
   return 0;
}
