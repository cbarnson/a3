#ifndef __CONFIG_H
#define __CONFIG_H


const unsigned char MY_PI_NUMBER = 0x04;
const unsigned char MY_NODE_NUMBER = 0x02;

const uint8_t WILL_DEFAULT = 0x03;

const uint8_t NOT_NEIGH = 0x00;		// for when sending hello with empty neigh list
const uint8_t SYM_NEIGH = 0x01;		// for active neighbors
const uint8_t MPR_NEIGH = 0x02;		// for later, when MPR implemented

const uint8_t UNSPEC_LINK = 0x00;
const uint8_t SYM_LINK = 0x02;


#endif
