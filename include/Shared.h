#ifndef __HELPER_H
#define __HELPER_H

// defines
#ifndef USED_CLOCK
#define USED_CLOCK CLOCK_MONOTONIC_RAW // CLOCK_MONOTONIC_RAW if available
#endif

#ifndef NANOS
#define NANOS 1000000000LL
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <wchar.h>
#include <assert.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h> // gettimeofday
#include <string.h> // strerror
#include <errno.h> // errno
#include <unistd.h>
//#include <cstring>
#include <arpa/inet.h>

// other
#include "Constants.h"
#include "Types.h"

// emission intervals
/* const double HELLO_INTERVAL = 2.0;    // seconds */
/* const double REFRESH_INTERVAL = 2.0;  // seconds */
/* const double TC_INTERVAL = 5.0;       // seconds */
/* const double MID_INTERVAL = TC_INTERVAL; */
/* const double HNA_INTERVAL = TC_INTERVAL; */

/* // holding times */
/* const double NEIGHB_HOLD_TIME = 3 * REFRESH_INTERVAL; */
/* const double TOP_HOLD_TIME = 3 * TC_INTERVAL; */
/* const double DUP_HOLD_TIME = 30.0;    // seconds */
/* const double MID_HOLD_TIME = 3 * MID_INTERVAL; */
/* const double HNA_HOLD_TIME = 3 * HNA_INTERVAL; */

// for computing validity time "vtime" and "htime" fields
const double SCALING_FACTOR = 0.0625; // seconds

// helper functions
static double vtime_to_double(uint8_t t) {
   int b = (int)(bitset<4>(t).to_ulong());
   int a = (int)(t >> 4);
   // to be safe, pow unpredictable...
   int bb = 1;
   for (int i = 0; i < b; i++)
      bb *= 2;
   return (SCALING_FACTOR * (1.0 + ((double)a / 16)) * (double)bb);
}

static uint8_t double_to_vtime(double t) {
   assert(t >= 0);
   unsigned int b = 0;
   double tc_ratio = t / SCALING_FACTOR;
   while (tc_ratio >= pow(2, b)) {
      b++;
   }
   // want largest that satisfies the above, decrement once
   b--;
   // compute 16*(T/(C*(2^b))-1) and round up, this is 'a'
   double temp = ceil(16.0 * (t / (SCALING_FACTOR * ((double)pow(2, b))) - 1.0));
   unsigned int a = temp;
   // if 'a' is equal to 16, increment 'b' by one, and set 'a' to 0
   if (a == 16) {
      b++;
      a = 0;
   }
   // check
   assert(a >= 0 && a < 16);
   assert(b >= 0 && b < 16);

   unsigned char c = 0;
   c |= (a << 4);
   c |= b;
   return c;
}


#endif
