#ifndef __CONCRETEOLSRPACKETFACTORY_H
#define __CONCRETEOLSRPACKETFACTORY_H

#include <memory>
#include "OLSRPacketFactory.h"
#include "ConcreteOLSRPacket.h"

class ConcreteOLSRPacketFactory : public OLSRPacketFactory {
  public:
   std::shared_ptr<OLSRPacket> createOLSRPacket() {
      return std::make_shared<ConcreteOLSRPacket>();
   }
};

#endif
