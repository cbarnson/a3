#ifndef __CONCRETEOLSRPACKET_H
#define __CONCRETEOLSRPACKET_H

#include <cstdlib>
#include <stdint.h>
#include <cmath>
#include <bitset>

#include "IPAddr.h"
#include "OLSRPacket.h"
#include "Config.h"

class ConcreteOLSRPacket : public OLSRPacket {
public:
  int setHeader(unsigned char *buf, int buflen, int msg_type, IPAddr origin,
                int TTL, int hop_count, uint16_t psn, uint16_t msn,
                int msglen) {

    if (buflen - 16 < msglen)
      return -1;
      
    // clear the buffer
    memset(buf, 0, buflen);

    // packet length
    uint16_t packet_len = 16 + msglen;
    packet_len = htons(packet_len);
    memcpy(buf, &packet_len, sizeof(uint16_t));

    // packet sequence number
    psn = htons(psn);
    memcpy(buf+2, &psn, sizeof(uint16_t));

    // message type
    uint8_t mtype = msg_type;
    memcpy(buf+4, &mtype, sizeof(uint8_t));

    // set default vtime (6.0 seconds)
    uint8_t vtime = double_to_vtime(6.0);
    memcpy(buf+5, &vtime, sizeof(uint8_t));

    // set message size, note that param msglen is the size of the message
    // that follows the header (value excludes header)
    uint16_t msg_size = 12 + msglen;
    msg_size = htons(msg_size);
    memcpy(buf+6, &msg_size, sizeof(uint16_t));

    // set originator address
    IPAddr ip_addr(MY_PI_NUMBER, MY_NODE_NUMBER);
    uint32_t addr = ip_addr.to32bWord();
    addr = htonl(addr);
    memcpy(buf+8, &addr, sizeof(uint32_t));

    // set TLL to its default value, 1
    uint8_t ttl = (uint8_t)TTL;
    memcpy(buf+12, &ttl, sizeof(uint8_t));

    // set hop count
    uint8_t hop = (uint8_t)hop_count;
    memcpy(buf+13, &hop, sizeof(uint8_t));

    // set message sequence number
    msn = htons(msn);
    memcpy(buf+14, &msn, sizeof(uint16_t));

    return 16;
  }


  bool isValid(unsigned char* buf, int buflen, HeaderInfo& hi) {

     if (buflen < 16)
	return false;
     hi.headerLen = 16;
     
     uint16_t packet_len = 0;
     memcpy(&packet_len, buf, sizeof(uint16_t));
     packet_len = ntohs(packet_len);
     if (buflen < packet_len)
	return false;
     hi.dataLen = packet_len - 16;

     // if message type (byte 4) is not hello (value 1), return false
     uint8_t msg_type = 0;
     memcpy(&msg_type, buf+4, sizeof(uint8_t));
     if (msg_type != 1)
	return false;

     // if packet contains more than one message, return false
     uint16_t msg_size = 0;
     memcpy(&msg_size, buf+6, sizeof(uint16_t));
     msg_size = ntohs(msg_size);
     if (packet_len != (msg_size + 4))
	return false;

     // write originator address field into HeaderInfo
     uint32_t addr = 0;
     memcpy(&addr, buf+8, sizeof(uint32_t));
     addr = ntohl(addr);
     hi.org = IPAddr(addr);
     
     
     return true;
  }
  

  

private:
  const double SCALING_FACTOR = 0.0625;
  uint8_t double_to_vtime(double seconds) {
    unsigned int b = 0;

    double tc_ratio = seconds / SCALING_FACTOR;
    while (tc_ratio >= pow(2, b)) {
      b++;
    }
    // want largest that satisfies the above, decrement once
    b--;
    // compute 16*(T/(C*(2^b))-1) and round up, this is 'a'
    double temp = ceil(16.0 * (seconds / (SCALING_FACTOR * ((double)pow(2, b))) - 1.0));
    unsigned int a = temp;
    // if 'a' is equal to 16, increment 'b' by one, and set 'a' to 0
    if (a == 16) {
      b++;
      a = 0;
    }
    // check
    /*assert(a >= 0 && a < 16);
      assert(b >= 0 && b < 16);*/

    uint8_t c = 0;
    c |= (a << 4);
    c |= b;
    return c;
  }

  /*private:
  // for computing validity time "vtime" and "htime" fields
  const double SCALING_FACTOR = 0.0625; // seconds

  // helper functions
  double vtime_to_double(uint8_t t) {
  int b = (int)(bitset<4>(t).to_ulong());
  int a = (int)(t >> 4);
  // to be safe, pow unpredictable...
  int bb = 1;
  for (int i = 0; i < b; i++)
  bb *= 2;
  return (SCALING_FACTOR * (1.0 + ((double)a / 16)) * (double)bb);
  }

  uint8_t double_to_vtime(double t) {
  assert(t >= 0);
  unsigned int b = 0;
  double tc_ratio = t / SCALING_FACTOR;
  while (tc_ratio >= pow(2, b)) {
  b++;
  }
  // want largest that satisfies the above, decrement once
  b--;
  // compute 16*(T/(C*(2^b))-1) and round up, this is 'a'
  double temp = ceil(16.0 * (t / (SCALING_FACTOR * ((double)pow(2, b))) - 1.0));
  unsigned int a = temp;
  // if 'a' is equal to 16, increment 'b' by one, and set 'a' to 0
  if (a == 16) {
  b++;
  a = 0;
  }
  // check
  assert(a >= 0 && a < 16);
  assert(b >= 0 && b < 16);

  unsigned char c = 0;
  c |= (a << 4);
  c |= b;
  return c;
  }*/
};


#endif
