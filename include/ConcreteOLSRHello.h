// *** CPSC 4210/5210 Assignment 3 code
// ***** OLSRHello interface: creates the header of a Hello Message

#ifndef __CONCRETEOLSRHELLO_H
#define __CONCRETEOLSRHELLO_H

#include <vector>
#include "IPAddr.h"
#include "OLSRHello.h"
#include "Config.h"

// interface class; provide a concrete class implementing the member function.
// Note this is an instance of the Adapter design pattern: you will
// adapt your existing code that creates packet headers to the
// provided interface. 
class ConcreteOLSRHello : public OLSRHello {
  public:
   // assigns values according to the OLSR packet header to the
   // buffer buf.
   // buflen is the size of the buffer in bytes
   // mpr is a vector of IP addresses selected to be MPR's
   // nbours is a vector of IP addresses that are the neighbour nodes
   // RETURNS
   // the length of the entire message in bytes, or -1 if the buffer is not big
   // enough to store the message and the headers
   int setHeader(unsigned char * buf, int buflen, std::vector<IPAddr> mpr,
		 std::vector<IPAddr> nbours) {

      int len = (4 * mpr.size()) + (4 * nbours.size()) + 8;
      if (buflen < len)
	 return -1;		

      // clear buffer
      memset(buf, 0, buflen);

      // set willingness field to will default
      uint8_t willingness = WILL_DEFAULT;
      memcpy(buf+3, &willingness, sizeof(uint8_t));

      if (!nbours.empty()) {
	 /* uint8_t link_code = SYM_LINK; */
	 uint8_t link_code = 0x06;
	 memcpy(buf+4, &link_code, sizeof(uint8_t));
      } else {
	 /* uint8_t link_code = UNSPEC_LINK; */
	 uint8_t link_code = 0x00;
	 memcpy(buf+4, &link_code, sizeof(uint8_t));
      }

      uint16_t link_msg_size = len - 4;
      link_msg_size = htons(link_msg_size);
      memcpy(buf+6, &link_msg_size, sizeof(uint16_t));

      // copy each IP address in the neighbor list into the buffer
      int buf_index = 8;
      for (int i = 0; i < nbours.size(); i++) {
	 uint32_t addr = nbours[i].to32bWord();
	 addr = htonl(addr);
	 memcpy(buf+buf_index, &addr, sizeof(uint32_t));
	 buf_index += 4;
      }

      // return length of entire message in bytes
      return len;
   }


   bool isValid(unsigned char *buf, int msglen, int offset, HeaderInfo& hi) {

      uint8_t link_code = 0;
      uint16_t link_msg_size = 0;
      memcpy(&link_code, buf+offset, sizeof(uint8_t));
      memcpy(&link_msg_size, buf+offset+2, sizeof(uint16_t));
      link_msg_size = ntohs(link_msg_size);

      // false if there is not enough room specifed by msglen
      if (msglen < (link_msg_size + offset))
	 return false;

      // false if offset is not 0, 4, or some value larger than 4 but
      // less than or equal to msglen - 4 and it is positive
      if (!(offset == 0 || offset == 4 ||
	    (offset > 4 && offset <= (msglen - 4))))
	 return false;

      // false if link message size is > 4, and link code is not valid
      // (i.e. is not 0x06, which means SYM_NEIGH, SYM_LINK
      if (link_msg_size > 4 && link_code != 0x06)
	 return false;


      // ==========HeaderInfo fields==========
      // for each neighbor, which is of NeighborType::SYM, append
      // IPAddr entry to HeaderInfo's vector of IPAddr's
      int n = ((int)link_msg_size - 4) / 4;
      for (int i = 0; i < n; i++) {
	 int index = (i * 4) + offset + 4;
	 uint32_t addr = 0;
	 memcpy(&addr, buf+index, sizeof(uint32_t));
	 addr = ntohl(addr);
	 hi.neighbors.push_back(IPAddr(addr));	 
      }

      // set neighbortype field
      if (n > 0) {
	 hi.type = NeighborType::SYM;
      } else {
	 hi.type = NeighborType::NOTN;
      }

      // set LMOffsetEnd
      int lmoff = offset + (int)link_msg_size;
      hi.LMOffsetEnd = lmoff;
      
      
      return true;
   }


  
   ~ConcreteOLSRHello() {}
};

#endif
