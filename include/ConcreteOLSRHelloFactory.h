// *** CPSC 4210/5210 Assignment 3 code
// ***** OLSRHelloFactory interface: provides a function that
// ***creates an appropriate concrete OLSRHello object referenced
// ***via the OLSRHello interface; it is used in the test fixtures
// ***for OLSRHello.

#ifndef __CONCRETEOLSRHELLOFACTORY_H
#define __CONCRETEOLSRHELLOFACTORY_H

#include <memory>
#include "OLSRHello.h"
#include "OLSRHelloFactory.h"
#include "ConcreteOLSRHello.h"

class ConcreteOLSRHelloFactory : public OLSRHelloFactory {
public:
	// returns a shared_ptr to interface class IPUDPPacket.
	// The concrete object must create an object to a concrete IPUDPPacket.
	std::shared_ptr<OLSRHello> createOLSRHello() {
		return std::make_shared<ConcreteOLSRHello>();
	}

	//~ConcreteOLSRHelloFactory() {};
};

#endif