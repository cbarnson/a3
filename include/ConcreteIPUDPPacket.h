// *** CPSC 4210/5210 Assignment 3 code
// ***** IPUDPPacket interface: creates the combined header of IP/UDP packed

#ifndef __CONCRETEIPUDPPACKET_H
#define __CONCRETEIPUDPPACKET_H

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cassert>

#include <arpa/inet.h>

#include "IPUDPPacket.h"

class ConcreteIPUDPPacket : public IPUDPPacket {
  public:
   // Can provide any constructors needed; the sample relies on the
   // default constructor supplied by the compiler

   // concrete implementation for the header routine
   int setHeader(unsigned char * buf, int buflen, IPAddr src,
		 IPAddr dst, int TTL, int datalen) {
      // we don't do much, just fill the data length part of the header,
      // as demo.

      // first check if the buffer is big enough
      if (buflen < datalen+28)
	 return -1;

      // clear the buffer
      memset(buf, 0, buflen);  // we clear the buffer first!

      // set first 2 fields in first 8 bits
      buf[0] = 0x45;

      // set total length = 28 + datalen in 3rd and 4th bytes
      uint16_t total_len = 28 + datalen;
      total_len = htons(total_len);
      memcpy(buf+2, &total_len, sizeof(uint16_t));

      // set TTL to 1
      uint8_t ttl = 0x01;
      buf[8] = ttl;

      // set protocol to 138
      uint8_t protocol = 138;
      buf[9] = protocol;

      // set source IP address field
      uint32_t src_ip = src.to32bWord();
      src_ip = htonl(src_ip);
      memcpy(buf+12, &src_ip, sizeof(uint32_t));

      // set destination address field
      uint32_t dst_ip = dst.to32bWord();
      dst_ip = htonl(dst_ip);
      memcpy(buf+16, &dst_ip, sizeof(uint32_t));

      // compute and set checksum in bytes 10 and 11
      uint16_t arr[10];
      memset(arr, 0, sizeof(arr));      
      memcpy(arr, buf, 20);
      arr[5] = 0; // checksum for compution is 0
      uint32_t sum = 0;
      for (int i = 0; i < 10; i++) {
	 sum += (uint32_t)arr[i];
	 if ((sum >> 16) & 1) {
	    sum &= ~(1 << 16);
	    sum += 1;
	 }
      }
      uint16_t checksum = (uint16_t)sum;
      checksum = ~(checksum);
      memcpy(buf+10, &checksum, sizeof(uint16_t));

      // UDP HEADER

      // source port
      uint16_t src_port = 698;
      src_port = htons(src_port);
      memcpy(buf+20, &src_port, sizeof(uint16_t));

      // destination port
      uint16_t dst_port = 698;
      dst_port = htons(dst_port);
      memcpy(buf+22, &dst_port, sizeof(uint16_t));

      // length; header plus datalen NOTE: assuming header length implies
      // the UDP header len (8 bytes), not the whole header
      uint16_t udp_len = 8 + datalen;
      udp_len = htons(udp_len);
      memcpy(buf+24, &udp_len, sizeof(uint16_t));
      
      /* *(reinterpret_cast<uint16_t*>(buf+2)) = 28+datalen;  // set the packet */
      // length field
      return 28;  // the size of the header in bytes
   }

   bool isValid(unsigned char *buf, int buflen, HeaderInfo& hi) {
      if (buflen < 28)
	 return false;

      // set header length to 28
      hi.headerLen = 28;

      // set dataLen using IPUDP header total length field
      uint16_t ipudp_total_len = 0;
      memcpy(&ipudp_total_len, buf+2, sizeof(uint16_t));
      ipudp_total_len = ntohs(ipudp_total_len);
      hi.dataLen = (int)ipudp_total_len - 28; 
      
      // fail if UDP field source port is not 698
      uint16_t src_port = 0;
      memcpy(&src_port, buf+20, sizeof(uint16_t));
      src_port = ntohs(src_port);
      if (src_port != 698)
	 return false;

      // fail if UDP field destination port is not 698
      uint16_t dst_port = 0;
      memcpy(&dst_port, buf+22, sizeof(uint16_t));
      dst_port = ntohs(dst_port);
      if (dst_port != 698)
	 return false;

      // fail if IP field TTL is not 1, else set hi.TTL to one
      uint8_t ttl = 0;
      memcpy(&ttl, buf+8, sizeof(uint8_t));
      if (ttl != 1)
	 return false;
      hi.TTL = 1;

      // fail if checksum is not correct      
      uint16_t checksum_actual = 0;
      memcpy(&checksum_actual, buf+10, sizeof(uint16_t));
      uint16_t checksum_computed = ip_checksum(buf, 20);
      if (checksum_actual != checksum_computed)
      	 return false;
      
      // set source ip address
      uint32_t src_ip = 0;
      memcpy(&src_ip, buf+12, sizeof(uint32_t));
      src_ip = ntohl(src_ip);
      hi.src = IPAddr(src_ip);
      
      // set destination ip address
      uint32_t dst_ip = 0;
      memcpy(&dst_ip, buf+12, sizeof(uint32_t));
      dst_ip = ntohl(dst_ip);
      hi.src = IPAddr(dst_ip);
      
      return true;
   }

  private:
   uint16_t ip_checksum(unsigned char *buf, size_t hdr_len) {
      unsigned long sum = 0;
      /* uint16_t *ip1; */

      uint16_t arr[10];
      memset(arr, 0, sizeof(arr));
      memcpy(arr, buf, 20);
      arr[5] = 0; // checksum for compution is 0
    
      /* ip1 = buf; */
      int i = 0;
      while (hdr_len > 1)
      {
   	 /* sum += *ip1++; */
   	 sum += arr[i++];
   	 if (sum & 0x80000000)
   	    sum = (sum & 0xFFFF) + (sum >> 16);
   	 hdr_len -= 2;
      }
      
      while (sum >> 16)
   	 sum = (sum & 0xFFFF) + (sum >> 16);
      
      return(~sum);
   }

  
};

#endif
